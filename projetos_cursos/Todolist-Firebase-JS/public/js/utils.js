// Defindo referências para elementos da página
var authForm = document.getElementById('authForm')
var authFormTitle = document.getElementById('authFormTitle')
var register = document.getElementById('register')
var access = document.getElementById('access')

var auth = document.getElementById('auth')
var userContent = document.getElementById('userContent')
var userEmail = document.getElementById('userEmail')
var userID = document.getElementById('userID')

var sendVerification = document.getElementById('sendVerification')
var emailVerified = document.getElementById('emailVerified')

var passwordReset = document.getElementById('passwordReset')

var userImg = document.getElementById('userImg')
var userName = document.getElementById('userName')

var todoForm = document.getElementById('todoForm')

var todoCount = document.getElementById('todoCount')
var ulTodoList = document.getElementById('ulTodoList')

// Alterar o formulário de autenticação para o cadastro de novas contas
function toggleToRegister() {
  authForm.submitAuthForm.innerHTML = 'Cadastrar conta'
  authFormTitle.innerHTML = 'Insira seus dados para se cadastrar'
  hideItem(register)
  showItem(access)
  hideItem(passwordReset)
}

// Alterar o formulário de autenticação para o acesso de contas já existentes
function toggleToAccess() {
  authForm.submitAuthForm.innerHTML = 'Entrar'
  authFormTitle.innerHTML = 'Acesse a sua conta para continuar'
  hideItem(access)
  showItem(register)
  showItem(passwordReset)
}

// Simpplifica a exibição de elementos da página
function showItem(element) {
  element.style.display = 'block'
}

// Simplifica a remoção de elementos da página
function hideItem(element) {
  element.style.display = 'none'
}

// Exibir dados para o usuario autenticado
function showUserContent(user) {
  // Verificacao de email
  console.log(user)
    if (user.emailVerified){
      emailVerified.innerHTML = 'Email verificado'
      hideItem(sendVerification)
    } else{
      emailVerified.innerHTML = 'Email não verificado'
      showItem(sendVerification)
    }

    userName.innerHTML = user.displayName
    userImg.src = user.photoURL ? user.photoURL : 'img/unknownUser.png'
    userEmail.innerHTML = user.email
    userID.innerHTML = user.uid
    hideItem(auth)
    // Pega os dados do banco
    dbRefUsers.child(firebase.auth().currentUser.uid).on('value', function (dataSnapshot) {
      fillTodoList(dataSnapshot)
    })
    // Exibe os dados do usuario
    showItem(userContent)
}

// Mostrar autenticacao para nao autenticados
function showAuth() {
    authForm.email.value=''
    authForm.password.value=''
    hideItem(userContent)
    showItem(auth)
}


// Como vai ficar no React
{/* <Router>
    <PrivateRoute exact path="/" component={Home}/>
    <PrivateRoute exact path="/dashboard" component={dashboard}/>
    <Route exact path="/login" component={Login}/>
</Router> */}

// Verificacao de email, atributos extras
var actionCodeSettings = {
  url: 'https://login-1ffef.firebaseapp.com'
}

// Traduzir erros

function showError(prefix, error){
  console.log(error.code)
  switch (error.code) {
    case 'auth/invalid-email' : alert(prefix+ ' E-mail invalido.')
    break;

    case 'auth/wrong-password' : alert(prefix+ 'Senha inválida.')
    break;

    case 'auth/weak-password' : alert(prefix+ 'Senha deve ter pelo menos 6 caracteres.')
    break;

    case 'auth/email-already-in-use' : alert(prefix+ 'O e-mail ja esta em uso.')
    break;

    case 'auth/popup-closed-by-user' : alert(prefix+ 'O popup de autenticação foi fechado antes de concluir.')
    break;
    default: alert(prefix+ ' '+ error.message)

  
  }
}

var database = firebase.database()
var dbRefUsers = database.ref('users')