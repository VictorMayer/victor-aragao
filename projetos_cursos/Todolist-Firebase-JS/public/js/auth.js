// Idioma do projeto do firebase
firebase.auth().languageCode = 'pt-BR'




// Define entre registro e login
authForm.onsubmit = function (event) {
    event.preventDefault()
    if (authForm.submitAuthForm.innerHTML == 'Entrar') {
      firebase.auth().signInWithEmailAndPassword(authForm.email.value, authForm.password.value).catch(function (error) {
        showError('Falha no acesso: ', error)
      })
    } else {
      firebase.auth().createUserWithEmailAndPassword(authForm.email.value, authForm.password.value).catch(function (error) {
        cshowError('Falha no cadastro: ', error)
      })
  
    }
  }
// Efetua a autenticacao
  firebase.auth().onAuthStateChanged(function (user){
      if (user) {
          showUserContent(user)
      } else {
        showAuth()
    }
  })

//Logout
function singOut() {
    firebase.auth().signOut().catch(function (error) {
        showError('Falha ao sair da conta: ', error)
    })
}

// Verificacao de email
function sendEmailVerification() {
  var user = firebase.auth().currentUser
  user.sendEmailVerification(actionCodeSettings).then(function(){
    alert('O e-mail de verificação foi enviado '+ user.email)
  }).catch(function (error){
    showError('Falha ao enviar mensagem de verificação: ', error)
    console.log(error)
  })
}

// Alterar senha do usuario
function sendPasswordResetEmail() {
  var email = prompt('Redefinir senha! Informe o seu email. ', authForm.email.value)
  if (email) {
    firebase.auth().sendPasswordResetEmail(email, actionCodeSettings).then(function(){
      alert('Email de redefinição de senha foi enviado para ' +email)
    }).catch(function(error) {
      showError('Falha ao enviar mensagem de redefinição de senha: ', error)
      console.log(error)
    })
  } else {
    alert('Preencha o campo do email')
  }
  
}

// Login pelo google
function signInWithGoogle() {
  firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider()).catch(function(error){
    showError('Falha ao autenticar com o Google: ', error)
  })
}

// Login pelo Facebook
// function singInWithFacebook(){
//   firebase.auth().signInWithPopup(new firebase.auth.FacebookAuthProvider()).catch(function(error){
// showError('Falha ao autenticar com o Facebook: ', error)
//   })
// }

// Alterar nome do usuario 
function updateUserName(){
  var newUserName = prompt('Informe o novo nome de usuário. ', userName.innterHTML)
  if (newUserName && newUserName != ''){
    userName.innerHTML = newUserName
    firebase.auth().currentUser.updateProfile({
      displayName: newUserName
    }).catch(function(error) {
      showError('Houve um erro ao alterar o nome de usuário: ', error)
    })
  }else{
    alert('O nome de usuário não pode ser vazio')
  }
}

// Deletar conta
function deleteUserAccount(){
  var confirmation = confirm('Deseja excluir permanentemente a sua conta?')
  if (confirmation) {
    firebase.auth().currentUser.delete().then(function() {
      alert('Conta removida com sucesso')
    }).catch(function(error){
      showError('Houve um erro ao excluir a conta: ', error)
    })
  }else{

  }
}