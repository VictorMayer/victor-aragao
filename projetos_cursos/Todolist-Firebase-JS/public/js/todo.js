// Recebe a submit do form
todoForm.onsubmit = function(e){
    e.preventDefault()
    // Pega o valor do input e transforma em JSON
    if (todoForm.name.value != '') {
        var data = {
            name: todoForm.name.value
        }
        // Envia o JSON para o banco
        dbRefUsers.child(firebase.auth().currentUser.uid).push(data).then(function(){
            console.log('Tarefa '+ data.name+' adicionada com sucesso!')
            todoForm.reset()
        }).catch(function(error){
            showError('Falha ao adicionar tarefa: ', error)
        })
    }else{
        alert('O nome da tafera não pode estar vazio')        
    }
}

// Exibir a lista de tarefas do usuario

function fillTodoList(dataSnapshot) {
    ulTodoList.innerHTML = ''
    var num = dataSnapshot.numChildren()
    todoCount.innerHTML = num + (num > 1 ? ' tarefas' : ' tarefa') + ': ' //Exibe o numero de tarefas
    dataSnapshot.forEach(function(item) { //Percorre todos os elementos
        var value = item.val()

        var li = document.createElement('li') //Criar um LI
        var spanLi = document.createElement('span')

        spanLi.appendChild(document.createTextNode(value.name)) //Adiciona um texto
        li.appendChild(spanLi) //Adiciona o span dentro do LI
        li.classList.add('list-group-item')
        spanLi.id = item.key

        var liRemoveBtn = document.createElement('i')//cria o icone de Delete
        liRemoveBtn.classList.add('far', 'fa-trash-alt')//Adiciona as classes do botao
        liRemoveBtn.setAttribute('onclick', 'removeTodo(\"' + item.key + '\")')//adiciona o onclick do botao
        li.appendChild(liRemoveBtn)


        var liUpdateBtn = document.createElement('i')//cria o icone de Delete
        liUpdateBtn.classList.add('far', 'fa-edit')//Adiciona as classes do botao
        liUpdateBtn.setAttribute('onclick', 'updateTodo(\"' + item.key + '\")')//adiciona o onclick do botao
        li.appendChild(liUpdateBtn)
        

        ulTodoList.appendChild(li) //Adiciona o li dentro da UL

    })
}

//Remove tarefas

function removeTodo(key) {

    dbRefUsers.child(firebase.auth().currentUser.uid).child(key).remove().catch(function(error){
        showError('Falha ao remover, ', error)
    })
}

function updateTodo(key){
    var selectedItem = document.getElementById(key)
    var newTodoName = prompt('Escolha um novo nome para a tarefa: ', selectedItem.innerHTML)
    if (newTodoName != ''){
        var data = {
            name: newTodoName
        }
        dbRefUsers.child(firebase.auth().currentUser.uid).child(key).update(data)
    } else{
        alert('O nome da tarefa não pode ser vazio')
    }
}