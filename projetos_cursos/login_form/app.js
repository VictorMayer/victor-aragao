// Selectors
const login_btn = document.getElementById('login-btn')
const reg_btn = document.getElementById('register-btn')
const btn_overlay = document.getElementById('toggle-overlay')
const overlay = document.getElementById('overlay')
const text = document.getElementById('div-content')

// Login form
var email_log=document.getElementById('login-email')
var password_log=document.getElementById('login-password')
// Register form
var email_reg = document.getElementById('register-email')
var nome = document.getElementById('register-name')
var password_reg = document.getElementById('register-password')
var password_reg2 = document.getElementById('register-password2')
var number = document.getElementById('register-number')
var date = document.getElementById('register-date')



// Event Listeners

login_btn.addEventListener('click', () => {
    login();
    reset();
})

reg_btn.addEventListener('click', () => {
    register();
    reset();
})


// Functions
email_log.focus()

function login(){
    btn_overlay.classList.remove('register')
    overlay.classList.remove('register')
    reg_btn.classList.remove('register')
    login_btn.classList.remove('register')
    text.innerHTML = "<h2>Texto1</h2>"
}

function register(){
    btn_overlay.classList.add('register')
    overlay.classList.add('register')
    reg_btn.classList.add('register')
    login_btn.classList.add('register')
    text.innerHTML = "<h2>Texto2</h2>"
}

function validarVazio(x){
    if(x.value === ''){
        x.classList.add('is-invalid');
        x.classList.remove('is-valid');

    }
    else{
        x.classList.remove('is-invalid')
        x.classList.add('is-valid');
    }
}

function reset(){
    email_log.value = '';
    password_log.value = '';
    email_reg.value = '';
    nome.value = '';
    password_reg.value = '';
    password_reg2.value = '';
    number.value = '';
    date.value = '';
    email_log.classList.remove('is-valid', 'is-invalid')
    password_log.classList.remove('is-valid', 'is-invalid')
    nome.classList.remove('is-valid', 'is-invalid')
    email_reg.classList.remove('is-valid', 'is-invalid')
    password_reg.classList.remove('is-valid', 'is-invalid')
    password_reg2.classList.remove('is-valid', 'is-invalid')
    number.classList.remove('is-valid', 'is-invalid')
    date.classList.remove('is-valid', 'is-invalid')
}


