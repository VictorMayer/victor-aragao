import React from 'react'
import {Grid, Card, CardHeader, CardContent, Typography, IconButton, Avatar} from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert';
import TrendingUpOutlinedIcon from '@material-ui/icons/TrendingUpOutlined';
import TrendingDownOutlinedIcon from '@material-ui/icons/TrendingDownOutlined';
import NumberFormat from 'react-number-format'


function Cards(props) {
    return (
        <Grid item style={{width: '30%'}}>
            <Card>
                <CardHeader style={{textTransform: 'capitalize'}}
                avatar={
                    <Avatar style={props.type ? {backgroundColor: 'rgba(41,121,255,1)'} : {backgroundColor: 'rgba(255,23,68, 1)'}}>
                        {props.type ? <TrendingUpOutlinedIcon/> : <TrendingDownOutlinedIcon/>  }
                    </Avatar>
                }
                action={
                    <IconButton>
                        <MoreVertIcon/>
                    </IconButton>
                }
                title = {props.cardTitle}               
                />                
                <CardContent>
                    <Typography variant="body2" component="p">
                        <NumberFormat value={props.valor} displayType={'text'} thousandSeparator={true}  fixedDecimalScale={true} prefix={'R$'} />
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    )
}

export default Cards
