import React from 'react'
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import HistoryOutlinedIcon from '@material-ui/icons/HistoryOutlined';


const SidebarMenu = [
    {
        name: 'Início',
        path: '/home',
        icon: <HomeOutlinedIcon/>
    },

    {
        name: 'Histórico',
        path: '/history',
        icon: <HistoryOutlinedIcon/>
    }
]
export default SidebarMenu
