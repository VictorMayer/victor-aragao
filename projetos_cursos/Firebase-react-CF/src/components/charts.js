import React from 'react'
import {Doughnut} from 'react-chartjs-2'


export function MyDoughnut(props) {
    const data = {
        labels:['Entrada', 'Despesa'],
        datasets: [
            {
                data: [props.entry, props.expense],
                backgroundColor: [
                    'rgba(41,121,255,1)', 'rgba(255,23,68, 1)'
                ]
            }
        ]
        
    }
    const options = {
        maintainAspectioRatio : false,
        legend : {display: false},
        animation: {animateScale: true}
    }
    
    return (
        <div>
            <Doughnut data={data} options={options}/>
        </div>
    )
}

