import React from 'react'
import styled from 'styled-components'
import SidebarMenu from './sidebarmenus'
import {Link} from 'react-router-dom'



function Sidebar() {
    return (
        <SideBar>
            <Header>
            </Header>
            <Content style={{marginTop: '50px'}}>
                <Menu>
                    {
                        SidebarMenu.map((item, index) => {
                            return(
                                <Link style={linkStyle} to={item.path}>
                                    <MyMenuItem key={index}>
                                        <IconDiv>
                                            {item.icon}
                                        </IconDiv>
                                        <span style={{marginLeft: '20px'}}>
                                            {item.name}
                                        </span>
                                        </MyMenuItem>
                                    </Link>
                                
                            )
                        })
                    }
                </Menu>
            </Content>
        </SideBar>
    )
}
const SideBar = styled.div`
    width: 200px;
    position: fixed;
    left: 0;
    top: 0;
    height: 100%;
    background-color: rgba(5, 5, 5, 0.8);
    transition: 0.5s ease-in-out;

    @media(max-width: 1200px){
        left: -200px;
    }
`
const Header = styled.header`

`
const IconDiv = styled.span`

`
const Content = styled.div`

`
const Menu = styled.ul`
    margin: 0;
    padding: 10px;
`
const MyMenuItem = styled.li`
    list-style: none;
    width: 100%;
    padding: 10px 0;
    margin: 20px 0;
    transition: 0.3s ease-in-out;
    cursor: pointer;
    

    &:hover{
        background-color: rgba(41,121,255, 0.8)
    }
    

`
const linkStyle = {
    color: 'white',
    textDecoration: 'none'
}

export default Sidebar
