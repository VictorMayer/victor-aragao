import React, {useState} from 'react'
import app from '../firebase'
import {Grid,TextField, Button, Select, MenuItem, FormControl} from '@material-ui/core'
import styled from 'styled-components'
import AddIcon from '@material-ui/icons/Add';

const Form = () => {
    // State do objeto
    const[name, setName] = useState(' ')
    const[valor, setValor] = useState(0)
    const[type, setType] = useState(true)
    const userRef = app.database().ref('user')


    
    const adicionar = (e) => { 
        e.preventDefault()
        const data = {
            name,
            valor,
            type,
        }
        userRef.child(app.auth().currentUser.uid).push(data)
        setName('')
        setValor(0)
        setType(true)
    }
    return(
        <Grid item xs={12}>
                <MyForm onSubmit={e=>adicionar(e)}>
                <TextField label="Nome" type="text" variant="outlined" value={name} onChange={e=>setName(e.target.value)}/>
                <TextField label="Valor" type="number" variant="outlined" value={valor} onChange={e=>setValor(parseInt(e.target.value))}/>
                <FormControl variant="outlined">
                    <Select  value={type} onChange={e=> setType(e.target.value)}>
                        <MenuItem value={true}>Entrada</MenuItem>
                        <MenuItem value={false}>Saida</MenuItem>
                    </Select>
                </FormControl>
                <Button type="submit" variant="outlined"><AddIcon/></Button>
                </MyForm>
        </Grid>
    )
}
const MyForm = styled.form`
    display: flex;
    justify-content: space-around;
`

export default Form