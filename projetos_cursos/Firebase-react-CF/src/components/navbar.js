import React from 'react'
import {useHistory} from 'react-router-dom'
import {AppBar, IconButton, Toolbar, Typography} from '@material-ui/core'
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined'
import app from '../firebase'
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined'
import SidebarMenu from './sidebarmenus'
import styled from 'styled-components'

export const Navbar = (props) => {
    const user = app.auth().currentUser
    let history = useHistory()
    const handleClick = () => {
        history.push(`/perfil/${user.uid}`)
    }
 

    return(
        <nav>
            <AppBar position="fixed" style={{zIndex: '100'}}color="inherit">
                <Toolbar variant="dense">
                    <Typography variant="h6" style={{fontFamily: 'Sansita Swashed', fontSize: '30px', marginLeft: '10px'}} color="inherit">
                        CF
                    </Typography>
                        <Items>
                            {
                                SidebarMenu ? SidebarMenu.map((item, index) => {
                                    return(
                                        <Item onClick={()=> history.push(`${item.path}`)}>{item.name}</Item>
                                    )
                                }) : ''
                            }
                        </Items>
                    <div style={{marginLeft:'auto'}}>
                        <IconButton onClick={() => app.auth().signOut()} >
                            <ExitToAppOutlinedIcon style={{width: '25px', height: '25px'}} color="secondary"/>
                        </IconButton>
                        <IconButton style={{marginLeft: '20px'}} onClick={handleClick}>
                            {user.photoURL? <img style={{width: '40px', height: '40px', borderRadius: '100%'}} alt="" src= {user.photoURL}/> : <AccountCircleOutlinedIcon style={{width: '40px', height: '40px'}}/> }
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
        </nav>
    )
}

const Items = styled.ul`
    display: flex;
    list-style: none;
`
const Item = styled.li`
    display: none;
    transition: 0.5s ease-in-out;

    @media(max-width: 1200px){
        display: block;
        margin-left: 20px;
        height: 100%;
        cursor: pointer;
        &:hover{
            color: rgba(0, 0, 0, 0.8)
        }
    }
`

export default Navbar