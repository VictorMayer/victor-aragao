import { Table,TableHead, TableBody, TableCell, TableContainer, TableRow, TextField } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import app from '../firebase'
import {IconButton} from '@material-ui/core'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import EditIcon from '@material-ui/icons/Edit';

function List(){
    // Pegar dados para exibir
    const[list, setList] = useState()
    const userRef = app.database().ref('user')

    // Get item
    useEffect(() => {
        const userRef = app.database().ref('user')
        userRef.child(app.auth().currentUser.uid).on('value', (snapShot) => {
            const ops = snapShot.val()
            const List = []
            for (let id in ops){
                List.unshift({id, ...ops[id]})
            }
            setList(List)
        })
    },[])

    const remove = (key) => {
        userRef.child(app.auth().currentUser.uid).child(key).remove().catch((error) => {
            alert(error)
        })
    }

    const update = (key) => {
        var newName = prompt('Escolha um novo nome: ')
        var newValue = parseInt(prompt('Escolha o valor: '))
        var newData = {
            name: newName,
            valor: newValue
        }
        userRef.child(app.auth().currentUser.uid).child(key).update(newData)
    }

 
    

    return(
        <TableContainer style={HeadStyle}>
            <Table>
                <TableHead>
                    <TableRow >
                        <TableCell>Nome</TableCell>
                        <TableCell align="right">Valor</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        //op.id => id do firebase, op.name => nome, op.valor => preco
                        list
                        ? list.map((op) => (
                            <TableRow style={op.type  ? Entrada : Saida} key={op.id}>
                                <TableCell>
                                    <TextField disabled variant="outlined" value={op.name}/>
                                    </TableCell>
                                <TableCell align="right">
                                    <TextField disabled variant="outlined" value={op.valor}/>
                                    </TableCell>

                                <TableCell align="right">
                                    {/* icones para deletar e editar*/}
                                    <IconButton onClick={e=> update(op.id)}>
                                        <EditIcon style={{color: "rgb(41,121,255)"}}/>
                                    </IconButton>
                                    <IconButton onClick={e=> remove(op.id)}>
                                        <DeleteOutlineIcon style={{color: "rgb(255,23,68)"}}/>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        )) : <TableRow>
                            <TableCell>
                                Vazio
                            </TableCell>
                        </TableRow>
                    }
                </TableBody>
            </Table>
        </TableContainer>
    )
    


}

const Entrada = {
    borderLeft: '5px solid rgb(41,121,255)'
}
const Saida = {
    borderLeft: '5px solid rgb(255,23,68)'
}

export default List

const HeadStyle = {
    backgroundColor: 'white',
    boxShadow: '0px 0px 10px gray',
    padding: '20px',
}

