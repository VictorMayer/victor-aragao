import React from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Login from './pages/Login/login'
import Home from './pages/Home/home'
import {AuthProvider} from './auth'
import PrivateRoute from './privateroute'
import Perfil from './pages/Perfil/perfil'
import History from './pages/History/history'

const Routes = () => {
    
    return(
        <AuthProvider>
            <Router>
                <PrivateRoute exact path="/home" component={Home} />
                <PrivateRoute exact path="/perfil/:uid?" component={Perfil} />
                <Route exact path="/" component={Login} />
                <PrivateRoute exact path="/history" component={History} />
            </Router>
        </AuthProvider>
    )
}

export default Routes