import React from 'react'
import List from '../../components/items'
import {Grid} from '@material-ui/core'
import Navbar from '../../components/navbar'
import Sidebar from '../../components/sidebar'

function History() {
    return (
        <Grid>
        <Navbar Page="Histórico"/>        
        <Sidebar/>
        <Grid style={centerGrid} item xs={8}>
            <Grid item>
                <List/>
            </Grid>
        </Grid>
        </Grid>
    )
}

const centerGrid = {
    margin: '85px auto'
}
export default History
