import React, {useState, useCallback} from 'react'
import styled from 'styled-components'
import {Button, TextField, Grid} from '@material-ui/core'
import {withRouter} from 'react-router'
import app from '../../firebase'
import './style.css'

const RegisterForm = ({history, setLogin, login}) => {
    const[email, setEmail] = useState('')
    const[password, setPassword] = useState('')

    const handleSubmit = useCallback(async event => {
        event.preventDefault()
        try{
            await app.auth().createUserWithEmailAndPassword(email, password)
            history.push("/home")
        } catch(error){
            alert(error)
        }
    }, [history, email, password])

    return (
        <Grid item xs={12}>
            <Form onSubmit={handleSubmit}>
                <h2>Cadastro</h2>
                <Grid item xs={8} style={{margin: '40px auto'}}>
                    <TextField type="email" value={email} onChange={e=> setEmail(e.target.value)} variant="outlined" fullWidth label="Email" />
                </Grid>
                <Grid item xs={8} style={{margin: '40px auto'}}>
                    <TextField type="password" value={password} onChange={e=> setPassword(e.target.value)} variant="outlined" fullWidth label="Senha" />
                </Grid>
                <Grid item xs={8} style={{margin: '40px auto'}}>
                    <Button type="submit" variant="contained" color="primary">Criar conta</Button>
                </Grid>
                <Grid item xs={8} style={{margin: '40px auto'}} className="startHidden">
                    <Button variant="outlined" color="primary"  onClick={()=> setLogin(!login)}>Já possui uma conta?</Button>
                </Grid>
                
            </Form>
        </Grid>
    )
}
const Form = styled.form`

`

export default withRouter(RegisterForm)
