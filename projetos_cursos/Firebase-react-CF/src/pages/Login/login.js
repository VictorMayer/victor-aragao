import React, {useState} from 'react'
import {Grid, Button} from '@material-ui/core'
import styled from 'styled-components'
import LoginForm from './loginform'
import RegisterForm from './registerform'
import './style.css'


function Login() {
    
    const [login, setLogin] = useState(true)


    return (
        <Grid item xs={6} style={centerGrid} container>
            <Grid item xs={6} style={sideDiv} className="sideDiv">
                {
                    login ? 
                <div>
                    <H2>Olá, bem vindo de volta.<br/> </H2>
                    <h3>Faça login para continuar.</h3>
                    <h4>Não possui uma conta?</h4>
                    <Button variant="outlined" onClick={()=> setLogin(!login)}>Criar conta</Button> 
                    
                </div>
                     : 
                    <div>
                        <H2>Olá, seja bem vindo.<br/></H2>
                        <h3>Crie a sua conta para ter acesso.</h3>
                        <h4>Já possui uma conta?</h4>
                        <Button variant="outlined" onClick={()=> setLogin(!login)}>Fazer login</Button> 
                    </div>
                }
                <Grid item xs={12}>
                    <Img src="https://cdn.iconscout.com/icon/free/png-512/money-coin-2079602-1757685.png" />
                </Grid>
            </Grid>
            <Grid item className="formDiv">
                {login && <LoginForm login={login} setLogin={setLogin}/>}
                {!login && <RegisterForm login={login} setLogin={setLogin}/>}
            </Grid>
        </Grid>
    )
}
const H2 = styled.h2`
    color: black;
    text-align: center;
    line-height: 2
`
const Img = styled.img`
    margin: 5px auto;
    width: 300px;
`

const centerGrid = {
    margin: '50px auto',
    height: '600px',
    boxShadow: '0px 0px 20px gray',
    borderRadius: '20px',
    textAlign: 'center'
}
const sideDiv =  {
    backgroundImage: 'linear-gradient(to right, #4facfe 0%, #00f2fe 100%)',
    borderRadius: '20px 0 0 20px'
}

export default Login
