import React, {useState, useContext, useCallback} from 'react'
import {Grid, TextField, Button } from '@material-ui/core'
import styled from 'styled-components'
import app from '../../firebase'
import { withRouter, Redirect} from 'react-router'
import { AuthContext } from '../../auth'
import "./style.css"

const LoginForm = ({history, login, setLogin}) => {
    const[email, setEmail] = useState('')
    const[password, setPassword] = useState('')
    
    
    const handleSubmit = useCallback(
        async event => {
            event.preventDefault()
            try {
                await app.auth().signInWithEmailAndPassword(email, password)
                history.push("/home")
            } catch(error){
                alert(error)
            }
        },[history, email, password]
    )

    const {currentUser} = useContext(AuthContext)

    if (currentUser) {
        return <Redirect to="/home"/>
    }

    const loginGoogle = () => {
        app.auth().signInWithPopup(new app.auth.GoogleAuthProvider()).catch((error) => {
            alert('Falha ao autenticar, '+error)
        })
    }

    const PasswordReset = () => {
        var email = prompt('Informe o seu email:  ')
        if(email) {
            app.auth().sendPasswordResetEmail(email).then(()=> {
                alert('Email de redefinição de senha foi enviado para ' +email)
            }).catch((error)=> {
                alert(error)
            })
        }else{
            alert('Preencha o campo.')
        }
    }



    return (
        <Grid item xs={12}>
            <Form onSubmit={handleSubmit}>
                <h2>Login</h2>
                <Grid item xs={8} style={{margin: '40px auto'}}>

                    <TextField type="email" value={email} onChange={e=> setEmail(e.target.value)} variant="outlined" fullWidth label="Email" />
                </Grid>
                <Grid item xs={8} style={{margin: '40px auto'}}>
                    <TextField type="password" value={password} onChange={e=> setPassword(e.target.value)} variant="outlined" fullWidth label="Senha" />
                </Grid>
                <Grid item xs={8} style={{margin: '40px auto'}}>
                    <Button color="primary" type="submit" variant="contained">Entrar</Button>
                </Grid>
                
                <Grid item xs={8} style={{margin: '40px auto'}}>
                    <Button color="primary" variant="outlined" onClick={loginGoogle}>Entrar com conta Google</Button>
                </Grid>
                <Grid item xs={8} style={{margin: '40px auto'}}>
                    <Button variant="outlined" color="secondary" onClick={PasswordReset}>Esqueceu a senha? </Button>
                </Grid>
                <Grid item xs={8} style={{margin: '40px auto'}} className="startHidden">
                    <Button variant="outlined" color="primary" onClick={()=> setLogin(!login)}>Criar conta</Button>
                </Grid>
                
            </Form>
        </Grid>
            
    )
}
const Form = styled.form`


`

export default withRouter(LoginForm)
