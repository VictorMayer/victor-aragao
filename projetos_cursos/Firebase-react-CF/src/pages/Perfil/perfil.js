import React from 'react'
import app from '../../firebase'
import Navbar from '../../components/navbar'
import Sidebar from '../../components/sidebar'
import {Button, Grid} from '@material-ui/core'
import styled from 'styled-components'


const Perfil = () => {
    const userData = app.auth().currentUser

    const changeName = () => {
        const name = prompt('Insira seu nome: ')
        app.auth().currentUser.updateProfile({
            displayName: name
        }).catch((error)=> {    
            alert(error)
        })
    }
    const excluirConta = () => {
            app.auth().currentUser.delete().then(()=> {
                alert('Conta removida com sucesso.')
            }).catch((error)=> {
                alert(error)
            })
        }
    


    return(
        <Grid>
            <Navbar Page="Perfil"/>
            <Sidebar/>
            <Grid item xs={8} style={centerGrid}>
                <MyContainer>
                    {
                        userData.displayName ? 
                        <div>
                            <h2 style={{textTransform: 'capitalize'}}>{userData.displayName} </h2>
                            <Button variant="outlined" color="primary" onClick={changeName}>Alterar nome de usuário</Button>
                        </div>
                        :
                        
                        <div>
                            <h2>Olá, o seu nome de usuário ainda não foi registrado. </h2> <Button variant="outlined" color="primary" onClick={changeName}>Registrar nome de usuário</Button>
                        </div>
        
                    }
                    
                    <Footer>
                        <Button variant="outlined" color="secondary" onClick = {excluirConta}style={{marginBottom: '20px'}}>Excluir conta</Button>
                    </Footer>
                </MyContainer>
            </Grid>
        </Grid>
    )
}

const Footer = styled.div`
    position: absolute;
    bottom: 0;
    left: 0
    padding: 20px;
`
const centerGrid = {
    margin: '85px auto'
}
const MyContainer = styled.div`
    background-color: white;
    width: 100%;
    padding: 20px;
    height: 300px;
    box-shadow: 0px 0px 20px gray;
    position: relative;
`

export default Perfil