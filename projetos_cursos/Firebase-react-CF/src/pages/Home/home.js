import React, {useEffect, useState} from 'react'
import app from '../../firebase'
import Navbar from '../../components/navbar'
import Form from '../../components/itemform'
import {Grid} from '@material-ui/core'
import styled from 'styled-components'
import {MyDoughnut} from '../../components/charts'
import NumberFormat from 'react-number-format'
import SideBar from '../../components/sidebar'
import Cards from '../../components/cards'


const  Home = () => {
    const [list, setList] = useState()
    const [entry, setEntry] = useState(0)
    const [expense, setExpense] = useState(0)
    const [items, setItems] = useState('')
  

    const getTotal = (ops) => {
        var Entry = 0
        var Expense = 0
        var List = []
        ops.map((op) => {
            if (op.type === true) {
                Entry = Entry + (op.valor)
                List.unshift(op)
            }else {
                Expense = Expense + (op.valor)
                List.unshift(op)
            }
        })
        setEntry(entry + Entry)
        setExpense(expense + Expense)
        setList(List)
        let items = List.slice(0, 3)
        setItems(items)
    }

    useEffect(() => {
        const userRef = app.database().ref('user')
        userRef.child(app.auth().currentUser.uid).on('value', (snapShot) => {
            const ops = snapShot.val()
            const List = []
            for (let id in ops){
                List.push({id, ...ops[id]})
            }
            getTotal(List)
        })
    },[])



    return(
        <Grid>
            <Navbar Page="Home"/>
            <SideBar/>
            <Grid item xs={8} style={centerGrid}>
                <FormContainer>
                    <Form/>
                </FormContainer>     
                <MyContainer>
                    <Textdiv>
                        <h2>Entrada: </h2>
                        <h3><NumberFormat value={entry} displayType={'text'} thousandSeparator={true}  fixedDecimalScale={true} prefix={'R$'} /></h3>
                    </Textdiv>
                    <ChartContainer>
                        <div style={{height: '100%', width: '100%'}}>
                            <MyDoughnut style={chartStyle} entry={entry} expense={expense}/>
                        </div>
                    </ChartContainer>
                    <Textdiv style={{textAlign: 'right'}}>
                        <h2>Despesas:</h2>
                        <h3><NumberFormat value={expense} displayType={'text'} thousandSeparator={true}  fixedDecimalScale={true} prefix={'R$'} /></h3>
                    </Textdiv>
                </MyContainer>
                <h1>Suas últimas operações: </h1>
                <CardsContainer>                    
                    <Grid container spacing={3}>
                    {items ? items.map((op) => (
                        <Cards key={op.id} type={op.type}  cardTitle={op.name} valor={op.valor}/>
                    )) : ''

                    }
                    </Grid>
                </CardsContainer>
                
            
            

        </Grid>
        </Grid>
    )
}
const Textdiv = styled.div`
    display: flex;
    flex-direction: column
`

const MyContainer = styled.div`
    height: 400px;
    background: white;
    margin: 40px 0;
    box-shadow: 0px 0px 10px gray;
    padding: 20px;
    width: 100%;
    display: flex;

    @media(max-width: 700px){
        flex-direction: column
    }

`
const ChartContainer = styled.div`
    height: 100%;
    width: 60%;
    margin: auto
`
const FormContainer = styled.div`
    height: 50px;
    background-color: white;
    margin: 40px 0;
    box-shadow: 0px 0px 10px gray;
    padding: 20px;
    width: 100%;    
`
const centerGrid = {
    margin: '85px auto'
}
const chartStyle = {
    margin: 'auto'
}
const CardsContainer = styled.div`
    width: 100%;
    padding: 20px;
    display: flex;
    @media(max-width: 700px){
        flex-direction: column
    }
`

export default Home